package facci.catherinsantana.appexample;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView)findViewById((R.id.textView1));

        textView.animate().alpha(0f).setDuration(0);

                textView.animate().alpha(1f).setDuration(800);

        Log.i("MainActivity","Catherin Liliana Santana Delgado");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(MainActivity.this,ConvertidorActivity.class);
                startActivity(intent);
            }
        }, 3000);
    }
}