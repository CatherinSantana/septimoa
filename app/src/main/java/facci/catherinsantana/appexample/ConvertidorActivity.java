package facci.catherinsantana.appexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.view.View;
import android.view.Gravity;
import android.view.LayoutInflater;


public class ConvertidorActivity extends AppCompatActivity {
    private Spinner Menu1, Menu2;
    private  EditText Edt_Valor1,Edt_Valor2;
    float C,F,res;
    String valores1, valores2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor);

        /*Enlasamos los objetos de tipo EditText con los
        objetos definidos en el archivo XML, esto se hace llamando al metodo findViewById
         */
        Edt_Valor1=(EditText)findViewById(R.id.editText_Valor1);
        Edt_Valor2=(EditText)findViewById(R.id.editTex_Valor2);

        /*Declaramos un arreglo de tipo string y le colocamos los siguientes valores
         */
        final String[] datos =
                 new String[]{"seleccione el grado", "Centígrados", "Fahrenheit"};

        /*
        vamos a crear un adaptador de tipo ArrayAdapter para trabajar con un array generico de java
         */
        ArrayAdapter<String> adaptador=
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datos);

    /* Para enlazar nuestro adaptador (y por lo tanto nuestros datos a este control
    utilizamos el sisguiente codigo java */

        Menu1= (Spinner)findViewById(R.id.spinner_Menu1);
        Menu2= (Spinner)findViewById(R.id.spinner_Menu2);
        adaptador.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        Menu1.setAdapter(adaptador);
        Menu2.setAdapter(adaptador);

        /* obtenemos el dato seleccionado para este evento definimos dos metodos, el primero de ellos (onItemSelect) que sera
        llamado cada ez que se seleccionemos una opcion a la lista despegable, y el segundo (onNothingSelected) que se llamara cuando no haya
        ninguna opcion seleccionada (esto puede ocurrir por ejemplo si el adaptador no tiene datos
         */

        Menu1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               android.view.View v, int position, long id){

                        valores1= datos[position];
                    }
                    public void onNothingSelected(AdapterView<?> parent){

                    }
                });
        Menu2.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               android.view.View v, int position, long id){

                        valores2= datos[position];

                        //Condicion que evalua la variables valores
                        if (valores2== "seleccione el grado" && valores1 == "seleccione el gradoo"){

                        }else {
                            //Ahora varificamos si el editText se encuentra vacio
                            String vacio = Edt_Valor1.getText().toString();
                            if (vacio.equals("")) {
                                //Meto que manda a llamar al mensaje personalizado
                                MensajeIconToast(R.layout.toast, Toast.LENGTH_LONG);
                                Edt_Valor1.requestFocus();
                            } else {
                                if (valores1 == "Seleccione el grado") {
                                    Mensaje();
                                } else {
                                    //Condicion que evalua las variables valores
                                    if (valores1 == "Centígrados" && valores2 == "Fahrenheit") {
                                        /*obtenemos los datos de los objetos enlazados y los almacenamos en las variables de tipo FLOAT*/
                                        C = Float.parseFloat(Edt_Valor1.getText().toString());
                                        //Formula de Cetigrados a Fahrenheit
                                        res = (C * 1.8f) + 32;
                                        //Convertimos el resultado en un dato de tipo string
                                        String resultado = String.valueOf(res);
                                        //Mostramos el dato en el editText_Valor2
                                        Edt_Valor2.setText(resultado);
                                    }
                                    if (valores1 == "Fahrenheit" && valores2 == "Centígrados") {
                                        F = Float.parseFloat(Edt_Valor1.getText().toString());
                                        //Formula de Fahrenheit a Centigrados
                                        res = (F - 32) / 1.8f;
                                        String resultado = String.valueOf(res);
                                        Edt_Valor2.setText(resultado);
                                    }
                                }
                            }
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent){

                    }
                });
    }
    private void MensajeIconToast(int layout, int duration){
        //El LayoutInflater sirve para traer un layout hecho en XML como una vista en java

        LayoutInflater inflater = getLayoutInflater();
        View layoutView = inflater.inflate(layout, null);
        /* un toast es un mensaje que se muestra en pantalla durante unos segundos al usuario para luego volver a desaparecer
        automaticamente sin requerir ningun tipo de actuacion por su parte, y sin recibir el foco en ningun momento
         */
        Toast toast = new Toast(getApplicationContext());
        /*setGravity este acepta tres parametros: a constantes Gravedad, un desplazamiento de posicion x, y un deplazamiento y pocisiones
         */
        toast.setGravity(Gravity.CENTER_VERTICAL,0, 0 );
        /*setDurationestablecer la duracion de la notificacion*/
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setDuration(duration);
        toast.setView(layoutView);
        toast.show();
    }

    public void Mensaje(){
        Toast toast= Toast.makeText(this, "Seleccione un grado", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }
 }
